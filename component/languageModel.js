import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';

const Data = [
  {
    id: 1,
    first_name: 'English',
  },
  {
    id: 2,
    first_name: 'Hindi',
  },
  {
    id: 3,
    first_name: 'Punjabi',
  },
  {
    id: 4,
    first_name: 'Bengali',
  },
  {
    id: 5,
    first_name: 'Malayalam',
  },
  {
    id: 6,
    first_name: 'Tamil',
  },
  {
    id: 7,
    first_name: 'Kannad',
  },
  {
    id: 8,
    first_name: 'Urdu',
  },
  {
    id: 9,
    first_name: 'Gujrati',
  },
  {
    id: 10,
    first_name: 'Telugu',
  },
  {
    id: 10,
    first_name: 'Other',
  },
];
export class LanguageModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
      renderData: Data,
      visibleModal: this.props.showLanguageModel,
    };
  }

  onPressHandler(id) {
    let renderData = [...this.state.renderData];
    for (let data of renderData) {
      if (data.id == id) {
        data.selected = data.selected == null ? true : !data.selected;
        break;
      }
    }
    this.setState({renderData});
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '96%',
          backgroundColor: '#CED0CE',
          marginLeft: '5%',
        }}
      />
    );
  };

  closeModel = () => {
    this.props.getSelectedLanguage(this.state.renderData);
    this.props.handlePress();
    this.setState({visibleModal: false});
  };

  render() {
    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={this.state.visibleModal}
        onRequestClose={this.closeModel}>
        <View style={styles.container}>
          <View style={{flex: 0.4}}></View>
          <View style={styles.headerContainer}>
            <View style={styles.backButtonContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.closeModel();
                }}>
                <Text style={styles.backTextStyle}>Back</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                paddingVertical: 10,
                paddingLeft: Math.round(Dimensions.get('window').width) / 7,
              }}>
              <Text style={styles.headerTitle}>Select One</Text>
            </View>
          </View>

          <View style={styles.flatListContainer}>
            <Text style={{fontSize: 20, fontWeight: '600'}}>
              Choose your Language(s)
            </Text>
          </View>
          <View>
            <FlatList
              data={this.state.renderData}
              keyExtractor={(item) => item.id.toString()}
              showsHorizontalScrollIndicator={false}
              ItemSeparatorComponent={this.renderSeparator}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.onPressHandler(item.id)}>
                  <View style={styles.cardContainer}>
                    <View style={styles.leftLanguageContainer}>
                      <Text
                        style={
                          item.selected == true
                            ? {
                                marginLeft: '10%',
                                fontSize: 15,
                                color: '#3498DB',
                              }
                            : {marginLeft: '10%', fontSize: 15}
                        }>
                        {item.first_name}
                      </Text>
                    </View>
                    <View style={styles.rightCheckContainer}>
                      {item.selected == true ? (
                        <Image
                          style={styles.tinyLogo}
                          source={require('../checkMark.png')}
                        />
                      ) : (
                        <></>
                      )}
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  flatListContainer: {
    paddingLeft: 20,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: '#cccccc',
    borderTopColor: '#cccccc',
    height: 60,
    justifyContent: 'center',
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftLanguageContainer: {
    padding: 10,
    paddingVertical: 15,
    backgroundColor: 'white',
  },
  rightCheckContainer: {
    paddingRight: 20,
    paddingVertical: 15,
  },
  headerContainer: {flexDirection: 'row'},
  tinyLogo: {
    width: 20,
    height: 20,
    tintColor: '#3498DB',
  },
  backButtonContainer: {padding: 10, paddingLeft: '5%'},
  backTextStyle: {fontSize: 20, color: '#3498DB'},
  headerTitle: {fontSize: 20, fontWeight: '800'},
});
export default LanguageModel;
