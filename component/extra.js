import React, {PureComponent} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import ActionSheet from 'react-native-actionsheet';
import {Camera} from './component/camera';
import ImagePicker from 'react-native-image-picker';

export default class App extends PureComponent {
  constructor() {
    super();
    this.state = {
      videos: [],
      processing: false,
      recording: false,
      index: '',
      vidSource: '',
    };
  }

  async startRecording() {
    this.setState({recording: true});
    // default to mp4 for android as codec is not set
    const {uri, codec = 'mp4'} = await this.camera.recordAsync();
    this.setState({recording: false, vidSource: uri}, () => {
      this.uploadVideo();
    });

    // try {
    //   await fetch(ENDPOINT, {
    //     method: 'post',
    //     body: data,
    //   });
    // } catch (e) {
    //   console.error(e);
    // }
    // this.setState(
    //   {
    //     vidSource: uri,
    //   },
    //   () => {
    //     this.uploadVideo();
    //   },
    // );
    //this.setState({processing: false});
  }

  uploadVideo = async () => {
    this.setState({processing: false});
    const type = `video/mp4`;
    const data = new FormData();
    const uri = this.state.vidSource;
    data.append('video', {
      name: 'mobile-video-upload',
      type,
      uri,
    });
    // try {
    //   await fetch(ENDPOINT, {
    //     method: 'post',
    //     body: data,
    //   });
    // } catch (e) {
    //   console.error(e);
    // }
    alert('successful');
    this.setState({processing: false});
  };

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  stopRecording() {
    this.camera.stopRecording();
    this.setState({index: ''});
  }

  openGallery = () => {
    //alert('1');
    const options = {
      title: 'Video Picker',
      takePhotoButtonTitle: 'Take Video...',
      mediaType: 'video',
      videoQuality: 'medium',
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        console.log(source);
        this.setState(
          {
            vidSource: source,
          },
          () => {
            this.uploadVideo();
          },
        );
      }
    });
  };

  actionButtonPressed = (index) => {
    this.setState({index: index});

    switch (index) {
      case 1:
        this.openGallery();
        break;

      case 0:
        break;

      case 2:
        break;

      default:
    }
  };

  cameraView = () => {
    const {recording, processing} = this.state;

    let button = (
      <TouchableOpacity
        onPress={this.startRecording.bind(this)}
        style={styles.capture}>
        <Text style={{fontSize: 14}}> RECORD </Text>
      </TouchableOpacity>
    );

    if (recording) {
      button = (
        <TouchableOpacity
          onPress={this.stopRecording.bind(this)}
          style={styles.capture}>
          <Text style={{fontSize: 14}}> STOP </Text>
        </TouchableOpacity>
      );
    }

    if (processing) {
      button = (
        <View style={styles.capture}>
          <ActivityIndicator animating size={18} />
        </View>
      );
    }

    return (
      <View
        style={{
          flex: 1,
          //right: -110,
          zIndex: 1,
        }}>
        <RNCamera
          ref={(ref) => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({barcodes}) => {}}
          captureAudio={true}
        />
        <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
          {button}
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.index === 0 ? this.cameraView() : <></>}
        <Text onPress={this.showActionSheet}>Open ActionSheet</Text>
        <ActionSheet
          ref={(o) => (this.ActionSheet = o)}
          title={'Which one do you like ?'}
          options={['Camera', 'Choose from Liberary', 'cancel']}
          cancelButtonIndex={2}
          destructiveButtonIndex={2}
          onPress={(index) => {
            /* do something */
            this.actionButtonPressed(index);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

//AppRegistry.registerComponent('App', () => ExampleApp);
