import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator,
  Text,
  TouchableOpacity,
} from 'react-native';
//Import Basic React Native Component
import Video from 'react-native-video';
//Import React Native Video to play video
import MediaControls, {PLAYER_STATES} from 'react-native-media-controls';

export class VideoPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: 'content',
      showChildModel: false,
    };
  }

  componentDidMount() {
    if (this.props && this.props.showModel) {
      let showChildModel = this.props.showModel;
      this.setState({showChildModel: showChildModel});
    }
  }

  onSeek = (seek) => {
    //Handler for change in seekbar
    this.videoPlayer.seek(seek);
  };

  onPaused = (playerState) => {
    //Handler for Video Pause
    this.setState({
      paused: !this.state.paused,
      playerState,
    });
  };

  onReplay = () => {
    //Handler for Replay
    this.setState({playerState: PLAYER_STATES.PLAYING});
    this.videoPlayer.seek(0);
  };

  onProgress = (data) => {
    const {isLoading, playerState} = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({currentTime: data.currentTime});
    }
  };

  onLoad = (data) => this.setState({duration: data.duration, isLoading: false});

  onLoadStart = (data) => this.setState({isLoading: true});

  onEnd = () =>
    this.setState({playerState: PLAYER_STATES.ENDED}, () => {
      // this.props.navigation.navigate('Video', {
      //   url: this.props.route.params.url,
      // });
      this.setState({showChildModel: false});
    });

  onError = () => alert('Oh! ', error);

  exitFullScreen = () => {
    alert('Exit full screen');
  };

  enterFullScreen = () => {};

  onFullScreen = () => {
    if (this.state.screenType == 'content')
      this.setState({screenType: 'cover'});
    else this.setState({screenType: 'content'});
  };
  renderToolbar = () => (
    <View>
      <Text> toolbar </Text>
    </View>
  );
  onSeeking = (currentTime) => this.setState({currentTime});

  render() {
    console.log('props', this.props);
    const {loading} = this.props;
    const {url} = this.props;
    console.log('url', url);
    return (
      <Modal
        transparent={true}
        animationType={'none'}
        visible={this.state.showChildModel}
        onRequestClose={() => {
          this.props.closeModel;
          this.setState({showModal: false});
          console.log('close modal');
        }}>
        <View style={styles.container}>
          <View style={{flex: 1}}>
            <Text>hi</Text>
          </View>
          <View
            style={{
              flex: 0.1,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignItems: 'flex-start',
              backgroundColor: 'black',
            }}>
            <TouchableOpacity style={styles.button}>
              <Text style={{color: 'white'}}>Accept</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}>
              <Text style={{color: 'white'}}>Reject</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
  button: {
    height: 40,
    backgroundColor: '#0A79DF',
    borderWidth: 0.3,
    borderColor: 'gray',
    width: '100%',
    borderRadius: 10,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default VideoPlayer;
