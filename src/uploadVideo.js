import React, {PureComponent} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
// import {RNCamera} from 'react-native-camera';
// import ActionSheet from 'react-native-actionsheet';
// import {Camera} from './component/camera';
import ImagePicker from 'react-native-image-picker';
import VideoPlayer from '../component/videoPlayer';
import LanguageModel from '../component/languageModel';

const url =
  'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      videos: [],
      processing: false,
      recording: false,
      index: '',
      videoSource: '',
      showModel: false,
      hidePreViewButton: true,
      showLanguageModel: false,
    };
  }

  // componentDidMount() {
  //   const url =
  //     this.props.route.params && this.props.route.params.url
  //       ? this.props.route.params.url
  //       : '';
  //   console.log('didmount', url);
  //   if (url) {
  //     this.setState({vidSource: url}, () => {
  //       this.uploadVideo();
  //     });
  //   } else {
  //     this.setState({vidSource: ''});
  //   }
  // }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   console.log('nextProps', nextProps);
  //   console.log('prevState', prevState);
  //   let url =
  //     nextProps.route && nextProps.route.params && nextProps.route.params.url;
  //   console.log(url);
  //   if (url !== '') {
  //     console.log('nextProps', url);
  //     //this.setState({vidSource: url});
  //     return {
  //       ...prevState,
  //       videoSource: url,
  //     };
  //   }

  //   return null;
  // }

  uploadVideo = async () => {
    this.setState({processing: true});
    const type = `video/mp4`;
    const data = new FormData();
    const uri = this.state.vidSource;
    data.append('video', {
      name: 'mobile-video-upload',
      type,
      uri,
    });
    //-----uncomment to impliment API ----------//
    // try {
    //   await fetch(ENDPOINT, {
    //     method: 'post',
    //     body: data,
    //   });
    // } catch (e) {
    //   console.error(e);
    // }
    Alert.alert('video uploaded Successfully');
    this.setState({processing: false});
  };

  uploadVideoPressed = () => {
    const options = {
      title: 'Video Picker',
      takePhotoButtonTitle: 'Take Video...',
      mediaType: 'video',
      videoQuality: 'medium',
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled video picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        //this.props.parentCallback(response.uri)
        console.log(response);
        this.setState(
          {
            videoSource: response.uri,
            hidePreViewButton: false,
          },
          () => {
            this.uploadVideo();
          },
        );
      }
    });
  };

  playPreview = () => {
    // this.props.navigation.navigate('Player', {url: url});
    this.setState({showModel: true});
    // if (this.state.videoSource) {
    //   //this.props.navigation.navigate('Player', {url: this.state.videoSource});
    // } else {
    //   alert('please upload a video first');
    // }
  };

  updateState = () => {
    this.setState({vidSource: url}, () => {
      this.uploadVideo();
    });
  };

  closeModel = () => {
    this.setState({loading: false});
  };

  deleteVideoPressed = () => {
    this.setState({hidePreViewButton: true});
  };

  selectLanguage = () => {
    this.setState({showLanguageModel: true});
  };

  handlePress() {
    this.setState({showLanguageModel: false});
  }

  //-----Get Selected Langage  here ------//
  getSelectedLanguage = async (modelData) => {
    console.log('languageObject', modelData);
    let selectedLanguage = [];

    await modelData.map((item, index) => {
      if (item.selected) {
        selectedLanguage.push(item.selectedLanguage);
      }
    });
    this.setState({selectedLanguageObject: modelData});
  };

  render() {
    return (
      <View style={styles.container}>
        {/* <View style={{paddingTop: 20}}>
          <TouchableOpacity
            style={styles.button}
            onPress={this.uploadVideoPressed}>
            <Text style={{color: 'white'}}>upload video</Text>
          </TouchableOpacity>
        </View> */}

        <View style={{paddingTop: 20}}>
          <TouchableOpacity style={styles.button} onPress={this.selectLanguage}>
            <Text style={{color: 'white'}}>Select language</Text>
          </TouchableOpacity>
        </View>

        {/* {this.state.videoSource !== '' ? (
          <View>
            <View style={{paddingTop: 20}}>
              <TouchableOpacity
                style={styles.button}
                onPress={this.playPreview}>
                <Text style={{color: 'white'}}>Preview</Text>
              </TouchableOpacity>
            </View>
            <View style={{paddingTop: 20}}>
              <TouchableOpacity
                style={styles.button}
                onPress={this.deleteVideoPressed}>
                <Text style={{color: 'white'}}>del video</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <></>
        )} */}

        {/* {this.state.showModel ? (
          <VideoPlayer
            showModel={this.state.showModel}
            url="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4"
            // closeModel={this.closeModel}
            // ref={(ref) => {
            //   this.child = ref;
            // }}
          />
        ) : (
          <></>
        )} */}

        {this.state.showLanguageModel ? (
          <LanguageModel
            showLanguageModel={this.state.showLanguageModel}
            handlePress={() => this.handlePress()}
            getSelectedLanguage={(modelData) =>
              this.getSelectedLanguage(modelData)
            }
          />
        ) : (
          <></>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: 40,
    backgroundColor: '#0A79DF',
    borderWidth: 0.3,
    borderColor: 'gray',
    width: '80%',
    borderRadius: 10,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

//AppRegistry.registerComponent('App', () => ExampleApp);
